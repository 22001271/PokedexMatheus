//
//  ListaViewController.swift
//  poukedex
//
//  Created by COTEMIG on 04/08/22.
//

import UIKit

class ListaViewController: UIViewController {
    
    class ViewController: UIViewController , UITableViewDataSource, UITableViewDelegate

    {
        
        var DataSource = self
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return pokemon.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? myCell
            
            let pokemons = pokemon[indexPath.row]
            
            cell?.nome.text = pokemons
            
            return cell ?? UITableViewCell()
        }
        
        
        
        
        var pokemon: [String] = []
        
        let cellIdentifier = "CellIdentifier"
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            
            
            pokemon = ["Bulbassauro", "Ivyssauro", "Venussauro", "Charmander", "Charmeleon", "Charizard", "Squirtle", "Wartortle", "Blastoise", "Pikachu"]

        }

    
    }
    

   
}
