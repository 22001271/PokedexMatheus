//
//  ViewController.swift
//  poukedex
//
//  Created by COTEMIG on 24/02/22.
//

import UIKit

class ViewController: UIViewController , UITableViewDataSource, UITableViewDelegate

{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemon.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? myCell
        
        let pokemons = pokemon[indexPath.row]
        
        cell?.nome.text = pokemons
        
        return cell ?? UITableViewCell()
    }
    
    var pokemon: [String] = []
    
    let cellIdentifier = "CellIdentifier"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func irParaPokedex(){
        
    }
    
    @IBAction func telaPokedex(_ sender: UIButton){
        irParaPokedex()
    }


}

